﻿using System;

namespace AtosLibrary.Infrastructure
{
    public class ReaderEntity
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Country { get; set; }
        public string City { get; set; }
        public bool IsDeleted { get; set; }

        public ReaderEntity(Guid id, string name, string surname, string country, string city, bool isDeleted = false)
        {
            Id = id;
            Name = name;
            Surname = surname;
            Country = country;
            City = city;
            IsDeleted = isDeleted;
        }
    }
}