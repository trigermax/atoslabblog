﻿using System;
using System.Collections.Generic;

namespace AtosLibrary.Infrastructure
{
    public interface IInMemoryDb<T>
    {
        T Get(Guid id);
        IEnumerable<T> GetAll();
        void Add(T entity);
    }
}