﻿using System;
using System.Collections.Generic;

namespace AtosLibrary.Infrastructure
{
    public class BookEntity
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int Number { get; set; }
        public List<Guid> Readers { get; set; }

        public BookEntity(Guid id, string name, string description, int number)
        {
            Id = id;
            Name = name;
            Description = description;
            Number = number;
        }
    }
}