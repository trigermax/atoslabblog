﻿using System;
using System.Collections.Generic;
using AtosLibrary.Infrastructure;

namespace AtosLibrary.Presentation.Book
{
    public class BookPresentationRepository : IBookPresentationRepository
    {
        private readonly IInMemoryDb<BookEntity> _inMemory;

        public BookPresentationRepository(IInMemoryDb<BookEntity> inMemory)
        {
            _inMemory = inMemory;
        }

        public BookModel Get(Guid id)
        {
            var entity = _inMemory.Get(id);

            BookModel bookModel = new BookModel();

            bookModel.Id = entity.Id;
            bookModel.Number = entity.Number;
            bookModel.Name = entity.Name;
            bookModel.Description = entity.Description;

            return bookModel;
        }

        public IEnumerable<BookModel> GetList()
        {
            var entities = _inMemory.GetAll();

            var books = new List<BookModel>();

            foreach (var entity in entities)
            {
                var bookModel = new BookModel();

                bookModel.Id = entity.Id;
                bookModel.Number = entity.Number;
                bookModel.Name = entity.Name;
                bookModel.Description = entity.Description;

                books.Add(bookModel);
            }

            return books;
        }
    }
}