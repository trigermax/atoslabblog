﻿using System;

namespace AtosLibrary.Presentation.Book
{
    public class BookModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int Number { get; set; }
    }
}