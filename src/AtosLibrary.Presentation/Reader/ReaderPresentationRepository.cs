﻿using System;
using System.Collections.Generic;
using System.Linq;
using AtosLibrary.Infrastructure;

namespace AtosLibrary.Presentation.Reader
{
    public class ReaderPresentationRepository : IReaderPresentationRepository
    {
        private readonly IInMemoryDb<ReaderEntity> _inMemory;

        public ReaderPresentationRepository(IInMemoryDb<ReaderEntity> inMemory)
        {
            _inMemory = inMemory;
        }

        public ReaderModel Get(Guid id)
        {
            var entity = _inMemory.Get(id);

            return new ReaderModel 
            {
                Id = entity.Id,
                Surname = entity.Surname,
                Name = entity.Name,
                Country = entity.Country,
                City = entity.City
            };
        }

        public IEnumerable<ReaderModel> GetList()
        {
            var entities = _inMemory.GetAll().Where(x => !x.IsDeleted);

            return entities.Select(x => new ReaderModel 
            {
                Id = x.Id,
                Surname = x.Surname,
                Name = x.Name
            }).ToList();
        }

        public IEnumerable<ReaderModel> GetBySurname(string surname)
        {
            if (surname is null)
                return new List<ReaderModel>();

            var entities = _inMemory.GetAll().Where(x => !x.IsDeleted && x.Surname.ToLowerInvariant().Contains(surname.ToLowerInvariant()));

            var readers = new List<ReaderModel>();

            foreach (var entity in entities)
            {
                var reader = new ReaderModel
                {
                    Id = entity.Id,
                    Surname = entity.Surname,
                    Name = entity.Name
                };

                readers.Add(reader);
            }

            return readers;
        }
    }
}