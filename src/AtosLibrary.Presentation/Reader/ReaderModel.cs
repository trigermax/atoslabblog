﻿using System;

namespace AtosLibrary.Presentation.Reader
{
    public class ReaderModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Country { get; set; }
        public string City { get; set; }
    }
}