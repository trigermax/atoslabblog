﻿using System;
using AtosLibrary.Infrastructure;

namespace AtosLibrary.Domain.Reader
{
    public class ReaderRepository : IReaderRepository
    {
        private readonly IInMemoryDb<ReaderEntity> _inMemory;

        public ReaderRepository(IInMemoryDb<ReaderEntity> inMemory)
        {
            _inMemory = inMemory;
        }

        public ReaderEntity Get(Guid id)
        {
            return _inMemory.Get(id);
        }

        public void Save(ReaderEntity reader)
        {
            _inMemory.Add(reader);
        }

        public void Update(Guid id, string name, string surname, string country, string city)
        {
            var entity = _inMemory.Get(id);

            entity.Name = name;
            entity.Surname = surname;
            entity.Country = country;
            entity.City = city;
        }

        public void Delete(Guid id)
        {
            var entity = _inMemory.Get(id);

            entity.Name = "";
            entity.Surname = "";
            entity.IsDeleted = true;
        }
    }
}