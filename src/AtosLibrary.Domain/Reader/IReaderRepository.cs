﻿using System;
using AtosLibrary.Infrastructure;

namespace AtosLibrary.Domain.Reader
{
    public interface IReaderRepository
    {
        ReaderEntity Get(Guid id);

        void Save(ReaderEntity reader);
        void Update(Guid id, string name, string surname, string country, string city);
        void Delete(Guid id);
    }
}