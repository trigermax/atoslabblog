﻿using System;

namespace AtosLibrary.Domain.Reader
{
    public interface IReaderFactory
    {
        Reader Create(string commandName, string commandSurname, string commandCountry, string commandCity);
        Reader Create(Guid id, IReaderRepository readerRepository);
    }
}