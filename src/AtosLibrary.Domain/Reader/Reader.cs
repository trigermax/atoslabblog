﻿using System;
using AtosLibrary.Infrastructure;

namespace AtosLibrary.Domain.Reader
{
    public class Reader
    {
        private readonly Guid _id;
        private readonly string _name;
        private readonly string _surname;
        private readonly string _country;
        private readonly string _city;


        internal Reader(Guid id, IReaderRepository readerRepository)
        {
            _id = id;

            var readerEntity = readerRepository.Get(id);

            _name = readerEntity.Name;
            _surname = readerEntity.Surname;
            _country = readerEntity.Country;
            _city = readerEntity.City;
        }

        internal Reader(string name, string surname, string country, string city)
        {
            _id = Guid.NewGuid();
            _name = name;
            _surname = surname;
            _country = country;
            _city = city;
        }

        public void Register(IReaderRepository readerRepository)
        {
            readerRepository.Save(new ReaderEntity(_id, _name, _surname, _country, _city));
        }

        public void Update(string name, string surname, string country, string city, IReaderRepository readerRepository)
        {
            readerRepository.Update(_id, name, surname, country, city);
        }

        public void Delete(IReaderRepository readerRepository)
        {
            readerRepository.Delete(_id);
        }
    }
}