﻿using System;

namespace AtosLibrary.Domain.Reader
{
    public class ReaderFactory : IReaderFactory
    {
        public Reader Create(string name, string surname, string country, string city)
        {
            return new Reader(name, surname, country, city);
        }

        public Reader Create(Guid id, IReaderRepository readerRepository)
        {
            return new Reader(id, readerRepository);
        }
    }
}