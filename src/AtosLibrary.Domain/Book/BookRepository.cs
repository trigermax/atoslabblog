﻿using System;
using System.Collections.Generic;
using AtosLibrary.Infrastructure;

namespace AtosLibrary.Domain.Book
{
    public class BookRepository : IBookRepository
    {
        private readonly IInMemoryDb<BookEntity> _inMemory;

        public BookRepository(IInMemoryDb<BookEntity> inMemory)
        {
            _inMemory = inMemory;
        }

        public void Save(BookEntity bookEntity)
        {
            _inMemory.Add(bookEntity);
        }

        public BookEntity Get(Guid bookId)
        {
            return _inMemory.Get(bookId);
        }

        public IEnumerable<BookEntity> GetList()
        {
            return _inMemory.GetAll();
        }

        public void UpdateReservation(Guid bookId, List<Guid> readers)
        {
            var result = _inMemory.Get(bookId);
            result.Readers.AddRange(readers);
        }
    }
}