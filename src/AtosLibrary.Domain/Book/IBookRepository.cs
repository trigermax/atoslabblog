﻿using System;
using System.Collections.Generic;
using AtosLibrary.Infrastructure;

namespace AtosLibrary.Domain.Book
{
    public interface IBookRepository
    {
        void Save(BookEntity bookEntity);
        BookEntity Get(Guid bookId);
        IEnumerable<BookEntity> GetList();
        void UpdateReservation(Guid bookId, List<Guid> readers);
    }
}