﻿using System;
using System.Collections.Generic;
using AtosLibrary.Infrastructure;

namespace AtosLibrary.Domain.Book
{
    public class Book
    {
        private readonly Guid _id;
        private readonly string _name;
        private readonly string _description;
        private readonly int _number;
        private readonly List<Guid> _readers;

        internal Book(Guid bookId, IBookRepository bookRepository)
        {
            _id = bookId;
            var result = bookRepository.Get(bookId);
            _name = result.Name;
            _description = result.Description;
            _number = result.Number;
            _readers = result.Readers;
        }

        internal Book(string name, string description, int number)
        {
            _id = Guid.NewGuid();
            _name = name;
            _description = description;
            _number = number;
        }

        public void Register(IBookRepository bookRepository)
        {
            bookRepository.Save(new BookEntity(_id, _name, _description, _number));
        }

        public void Reserve(Guid readerId, IBookRepository bookRepository)
        {
            if (_readers.Contains(readerId))
            {
                throw new Exception("Is reserved by you.");
            }

            if (_number == _readers.Count)
            {
                throw new Exception("All copies are reserved or rented. Please try later.");
            }

            _readers.Add(readerId);

            bookRepository.UpdateReservation(_id, _readers);
        }
    }
}