﻿using System;

namespace AtosLibrary.Application.Features.EditReader
{
    public class EditReaderCommand
    {
        public Guid Id { get; }
        public string Name { get; }
        public string Surname { get; }
        public string Country { get; }
        public string City { get; }

        public EditReaderCommand(Guid id, string name, string surname, string country, string city)
        {
            Id = id;
            Name = name;
            Surname = surname;
            Country = country;
            City = city;
        }
    }
}