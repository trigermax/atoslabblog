﻿using System;

namespace AtosLibrary.Application.Features.DeleteReader
{
    public class DeleteReaderCommand
    {
        public DeleteReaderCommand(Guid id)
        {
            Id = id;
        }

        public Guid Id { get; }
    }
}