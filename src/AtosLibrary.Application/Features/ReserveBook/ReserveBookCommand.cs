﻿using System;

namespace AtosLibrary.Application.Features.ReserveBook
{
    public class ReserveBookCommand
    {
        public Guid BookId { get; set; }
        public Guid ReaderId { get; set; }
    }
}