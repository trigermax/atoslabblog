using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AtosLibrary.Application.Features.DeleteReader;
using AtosLibrary.Application.Features.EditReader;
using AtosLibrary.Application.Features.RegistrationBook;
using AtosLibrary.Application.Features.RegistrationReader;
using AtosLibrary.Application.Features.ReserveBook;
using AtosLibrary.Application.Infrastructure;
using AtosLibrary.Domain.Book;
using AtosLibrary.Domain.Reader;
using AtosLibrary.Infrastructure;
using AtosLibrary.Presentation.Book;
using AtosLibrary.Presentation.Reader;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace AtosLibrary.WebHost
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddRazorPages();
            services.AddScoped<ICommandHandler<RegistrationBookCommand>, RegistrationBookCommandHandler>();
            services.AddScoped<ICommandHandler<RegistrationReaderCommand>, RegistrationReaderCommandHandler>();
            services.AddScoped<ICommandHandler<ReserveBookCommand>, ReserveBookCommandHandler>();
            services.AddScoped<ICommandHandler<EditReaderCommand>, EditReaderCommandHandler>();
            services.AddScoped<ICommandHandler<DeleteReaderCommand>, DeleteReaderCommandHandler>();

            services.AddScoped<IBookFactory, BookFactory>();
            services.AddScoped<IBookRepository, BookRepository>();
            services.AddScoped<IReaderFactory, ReaderFactory>();
            services.AddScoped<IReaderRepository, ReaderRepository>();

            services.AddSingleton<IInMemoryDb<BookEntity>, InMemoryBook>();
            services.AddSingleton<IInMemoryDb<ReaderEntity>, InMemoryReader>();

            services.AddScoped<IReaderPresentationRepository, ReaderPresentationRepository>();
            services.AddScoped<IBookPresentationRepository, BookPresentationRepository>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapRazorPages();
            });
        }
    }
}
