﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AtosLibrary.Application.Features.DeleteReader;
using AtosLibrary.Application.Features.EditReader;
using AtosLibrary.Application.Infrastructure;
using AtosLibrary.Presentation.Reader;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace AtosLibrary.WebHost
{
    public class ReaderIndexModel : PageModel
    {
        private readonly IReaderPresentationRepository _presentationRepository;
        private readonly ICommandHandler<DeleteReaderCommand> _commandHandler;

        public ReaderIndexModel(IReaderPresentationRepository presentationRepository, ICommandHandler<DeleteReaderCommand> commandHandler)
        {
            _presentationRepository = presentationRepository;
            _commandHandler = commandHandler;
        }

        public IList<ReaderModel> Readers { get; set; }

        [BindProperty]
        public string SearchText { get; set; }

        public void OnGet(string SearchText)
        {
            if (SearchText is null)
                Readers = _presentationRepository.GetList().ToList();
            else
                Readers = _presentationRepository.GetBySurname(SearchText).ToList();
        }

        public IActionResult OnPostDelete(Guid id)
        {
            _commandHandler.Handle(new DeleteReaderCommand(id));

            return RedirectToPage("/ReaderIndex", new { SearchText });
        }

        public IActionResult OnPostSearch()
        {
            return RedirectToPage("/ReaderIndex", new { SearchText });
        }
    }
}